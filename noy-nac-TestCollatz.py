#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, init_cache, RANGE_MAP_1000

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase): #pragma: no cover

    """
    def setUp(self):
        if not RANGE_MAP_1000:
            init_cache()  
    """

    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "1 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 1)

    def test_read_3(self):
        s = "999999 999999\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 999999)
        self.assertEqual(j, 999999)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)

    def test_eval_6(self):
        v = collatz_eval(49, 49)
        self.assertEqual(v, 25)

    def test_eval_7(self):
        v = collatz_eval(64, 64)
        self.assertEqual(v, 7)

    def test_eval_8(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)
    
    def test_eval_9(self):
        v = collatz_eval(5108, 12669)
        self.assertEqual(v, 268) 

    def test_eval_10(self):
        v = collatz_eval(3000, 2000)
        self.assertEqual(v, 217)

    def test_eval_11(self):
        v = collatz_eval(939002, 941000)
        self.assertEqual(v, 507)

    def test_eval_12(self):
        v = collatz_eval(798001, 800000)
        self.assertEqual(v, 375)

    def test_eval_13(self):
        v = collatz_eval(5001, 8000)
        self.assertEqual(v, 262)

    def test_eval_14(self):
        v = collatz_eval(9365, 7821)
        self.assertEqual(v, 260)

    def test_eval_15(self):
        v = collatz_eval(57, 1057)
        self.assertEqual(v, 179)

    def test_eval_16(self):
        v = collatz_eval(472, 868)
        self.assertEqual(v, 171)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 3, 4, 5)
        self.assertEqual(w.getvalue(), "3 4 5\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 80, 59, 12)
        self.assertEqual(w.getvalue(), "80 59 12\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("10 1\n49 49\n64 64\n1 999999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "10 1 20\n49 49 25\n64 64 7\n1 999999 525\n")

    def test_solve_3(self):
        r = StringIO("5 2555\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "5 2555 209\n")

    def test_solve_4(self):
        r = StringIO("5108 12669\n3000 2000\n939002 941000\n798001 800000\n5001 8000\n9365 7821\n57 1057\n472 868\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "5108 12669 268\n3000 2000 217\n939002 941000 507\n798001 800000 375\n5001 8000 262\n9365 7821 260\n57 1057 179\n472 868 171\n")

    def test_solve_5(self):
        r = StringIO()
        w = StringIO("test that this is unmodified")
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "test that this is unmodified")
        
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
#comment

